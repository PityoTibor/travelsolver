﻿namespace AoI.TravelSolver.DataTransferObjects
{
    public class LatLngDto
    {

        public LatLngDto(double xCoordinate, double yCoordinate)
        {
            this.Lng = xCoordinate;
            this.Lat = yCoordinate;
        }

        // TODO: check how to handle it in .NET
        // X
        public double Lng { get; set; }

        // Y
        public double Lat { get; set; }
    }
}