﻿namespace AoI.TravelSolver.DataTransferObjects
{
    public class AddressDto
    {
        public double xCoordinate { get; set; }
        public double yCoordinate { get; set; }
    }
}
