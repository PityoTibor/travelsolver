﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.DataTransferObjects.Json_Classes
{
    public class MatrixRequestDto
    {
        [JsonProperty(PropertyName = "locations")]
        public IEnumerable<double[]> locations { get; set; }

        [JsonProperty(PropertyName = "metrics")]
        public string[] metrics { get; set; }

        [JsonProperty(PropertyName = "units")]
        public string units { get; set; }
    }
}
