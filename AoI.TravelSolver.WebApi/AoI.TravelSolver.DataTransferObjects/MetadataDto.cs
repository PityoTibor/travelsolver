﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.DataTransferObjects
{
    public class MetadataDto
    {
        public string Attribution { get; set; }

        public string Service { get; set; }

        public long Timestamp { get; set; }

        public QueryDto Query { get; set; }

        public EngineDto Engine { get; set; }
    }

}
