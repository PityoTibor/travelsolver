﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.DataTransferObjects
{
    public class DestinationDto
    {
        public IEnumerable<double> Location { get; set; }

        [JsonProperty("snapped_distance")]
        public double Snappedistance { get; set; }
    }
}
