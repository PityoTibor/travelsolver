﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.DataTransferObjects
{
    public class ORSMatrixResultDto
    {
        public IEnumerable<double[]> Distances { get; set; }

        public IEnumerable<DestinationDto> Destinations { get; set; }

        public IEnumerable<DestinationDto> Sources { get; set; }

        public MetadataDto Metadata { get; set; }

        
    }
}
