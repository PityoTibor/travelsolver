﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.DataTransferObjects
{
    public class PlanRouteResultDto
    {
        public PlanRouteResultDto(IEnumerable<LatLngDto> latLngs, double distance)
        {
            this.LatLngs = latLngs;
            this.Distance = distance;
        }
        public IEnumerable<LatLngDto> LatLngs { get; set; }

        public double Distance { get; set; }
    }
}
