﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.DataTransferObjects
{
    public class QueryDto
    {
        public IEnumerable<double[]> Locations { get; set; }
        public string Profile { get; set; }
        public string ResponseType { get; set; }

        public IEnumerable<string> MetricsStrings { get; set; }

        public IEnumerable<string> Metrics { get; set; }

        public string Units { get; set; }
    }
}
