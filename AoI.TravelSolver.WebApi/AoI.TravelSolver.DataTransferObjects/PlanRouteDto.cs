﻿using AoI.TravelSolver.DataTransferObjects.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.DataTransferObjects
{
    public class PlanRouteDto : IPlanRouteDto
    {
        public IEnumerable<AddressDto> Addresses { get; set; }

        public AddressDto MainAddress { get; set; }
    }
}
