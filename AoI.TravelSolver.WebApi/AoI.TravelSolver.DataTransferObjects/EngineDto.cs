﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.DataTransferObjects
{
    public class EngineDto
    {
        public string Version { get; set; }

        [JsonProperty("build_date")]
        public DateTime BuildDate { get; set; }

        [JsonProperty("graph_date")]
        public DateTime GraphDate { get; set; }
    }
}
