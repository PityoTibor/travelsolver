﻿using AoI.TravelSolver.DataTransferObjects;
using AoI.TravelSolver.WebApi.Services;
using AoI.TravelSolver.WebApi.WebSite.Configuration;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.WebSite.Controllers
{
    [Route("api/routes")]
    [ApiController]
    public class RoutesController : ControllerBase
    {
        //public RoutesController(IGeneticAlgorithmService geneticAlgorithmService)
        //{

        //}
        public ActionResult<PlanRouteResultDto> PlanRoute([FromBody] PlanRouteDto planRouteDto)
        {
            IGeneticAlgorithmService service = new GeneticAlgorithmService();
            return Ok(service.StartGeneticAlgorithm(planRouteDto));
            
            //Address addr = new Address();
            //Address MainAddress = new Address(planRouteDto.MainAddress.xCoordinate, planRouteDto.MainAddress.yCoordinate);
            //addr.GetRequestAddressesArray[addr.GetRequestAddressesArray.Length] = MainAddress; // ezt egy függvénybe kell tenni, átadva a maint, esetleg a függvényben eltárolni.

            ////Address[] RequestAddressesArray = Address.GetRequestAddressesArray;
            ////double[,] DistanceMatrix = GetMatrixResult.GetDistanceMatrix(RequestAddressesArray);

            //ComputeGeneticAlgorithm cga = new ComputeGeneticAlgorithm();
            //cga.StartGeneticAlgorithm(AppSettingsHelper.Ga_Config.GeneticAlgorithmConfig, GetMatrixResult.GetDistanceMatrix(addr.GetRequestAddressesArray), MainAddress);

                
            ////ComputeGeneticAlgorithm.StartGeneticAlgorithm(AppSettingsHelper.Config.GeneticAlgorithmConfig, DistanceMatrix, MainAddress);

            //return Ok(cga.GetPlanRouteResult(MainAddress));
        }

        [HttpGet]
        public ActionResult<PlanRouteResultDto> PlanRoute()
        {
            ;
            var conf = AppSettingsHelper.Ga_Config.GeneticAlgorithmConfig;
            var conft = AppSettingsHelper.Ga_ItcmConfig.GeneticAlgorithmIsTerminationConditionMetConfig;

            GeneticAlgorithmisTerminationConditionMetConfiguration asd  = new GeneticAlgorithmisTerminationConditionMetConfiguration();
            var a = asd.GeneticAlgorithmIsTerminationConditionMetConfig;
            ;
            Console.WriteLine("asd");
            return null;


        }
    }
}
