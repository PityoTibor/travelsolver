﻿using AoI.TravelSolver.WebApi.Services;
using AoI.TravelSolver.WebApi.WebSite.Configuration;
using Microsoft.Extensions.Options;
using System;

public static class AppSettingsHelper
{
    static IServiceProvider services = null;

    public static IServiceProvider Services
    {
        get { return services; }
        set
        {
            ;
            if (services != null)
            {
                throw new Exception("Can't set once a value has already been set.");
            }
            services = value;
        }
    }

    public static GeneticAlgirithmConfiguration Ga_Config
    {
        get
        {
            //This works to get file changes.
            var s = services.GetService(typeof(IOptionsMonitor<GeneticAlgirithmConfiguration>)) as IOptionsMonitor<GeneticAlgirithmConfiguration>;
            GeneticAlgirithmConfiguration config = s.CurrentValue;

            return config;
        }
    }

    public static GeneticAlgorithmisTerminationConditionMetConfiguration Ga_ItcmConfig
    {
        get 
        {
            ;
            var s = services.GetService(typeof(IOptionsMonitor<GeneticAlgorithmisTerminationConditionMetConfiguration>)) as IOptionsMonitor<GeneticAlgorithmisTerminationConditionMetConfiguration>;
            GeneticAlgorithmisTerminationConditionMetConfiguration config = s.CurrentValue;

            return config;
        }
    }
}