﻿using AoI.TravelSolver.DataTransferObjects;
using AoI.TravelSolver.WebApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AoI.TravelSolver.WebApi.WebSite
{
    public class GeneticAlgorithmService : IGeneticAlgorithmService
    {
        public PlanRouteResultDto StartGeneticAlgorithm(PlanRouteDto planRouteDto)
        {
            Address addr = new Address();
            Address MainAddress = new Address(planRouteDto.MainAddress.xCoordinate, planRouteDto.MainAddress.yCoordinate);
            addr.GetRequestAddressesArray[addr.GetRequestAddressesArray.Length] = MainAddress; // ezt egy függvénybe kell tenni, átadva a maint, esetleg a függvényben eltárolni.

            //Address[] RequestAddressesArray = Address.GetRequestAddressesArray;
            //double[,] DistanceMatrix = GetMatrixResult.GetDistanceMatrix(RequestAddressesArray);

            ComputeGeneticAlgorithm cga = new ComputeGeneticAlgorithm();
            cga.StartGeneticAlgorithm(AppSettingsHelper.Ga_Config.GeneticAlgorithmConfig, GetMatrixResult.GetDistanceMatrix(addr.GetRequestAddressesArray), MainAddress);

            return cga.GetPlanRouteResult(MainAddress);
        }
    }
}
