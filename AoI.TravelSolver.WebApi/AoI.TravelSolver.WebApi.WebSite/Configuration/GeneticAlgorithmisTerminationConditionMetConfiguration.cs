﻿using AoI.TravelSolver.WebApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.WebSite.Configuration
{
    public class GeneticAlgorithmisTerminationConditionMetConfiguration
    {
        public GeneticAlgorithmIsTerminationConditionMetConfig GeneticAlgorithmIsTerminationConditionMetConfig { get; set; }
    }
}
