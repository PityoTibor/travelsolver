﻿using AoI.TravelSolver.DataTransferObjects;
using AoI.TravelSolver.DataTransferObjects.Json_Classes;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services
{
    public static class GetMatrixResult
    {
        public static double[,] GetDistanceMatrix(IAddress[] RequestAddressesArray)
        {
            ORSMatrixResultDto ORSMatrixResultDto = GetMatrixResult.GetORSMatrixResultDto(RequestAddressesArray);

            int numData = ORSMatrixResultDto.Metadata.Query.Locations.Count();
            double[,] distanceMatrix = new double[numData, numData];

            int x = 0;
            int y = 0;
            foreach (var item in ORSMatrixResultDto.Distances)
            {
                foreach (var value in item)
                {
                    distanceMatrix[x, y] = value;
                    y++;
                    if (y == numData)
                    {
                        y = 0;
                    }
                }
                x++;
            }

            return distanceMatrix;
        }

        static ORSMatrixResultDto GetORSMatrixResultDto(IAddress[] RequestAddressesArray)
        {
            IRestResponse response = GetAddresArrayWithYandXCoordinatePair(RequestAddressesArray);
            ORSMatrixResultDto ORSMatrixResultDto = JsonConvert.DeserializeObject<ORSMatrixResultDto>(response.Content);
            return ORSMatrixResultDto;
        }

        static IRestResponse GetAddresArrayWithYandXCoordinatePair(IAddress[] RequestAddressesArray)
        {
            double[][] array = new double[RequestAddressesArray.Length][];
            int i = 0;
            foreach (var address in RequestAddressesArray)
            {
                array[i] = new double[] { address.getXCoordinate, address.getYCoordinate };
                i++;
            }

            //for (int j = 0; j < RequestAddressesArray.Length; j++)
            //{
            //    array[i] = new double[] { RequestAddressesArray[i].getXCoordinate, RequestAddressesArray[i].getYCoordinate };
            //}

            var client = ApiRequests.Client();
            var request = ApiRequests.Request();
            return client.Execute(request.AddJsonBody(new MatrixRequestDto { locations = array, metrics = new string[] { "distance" }, units = "m" }));
        }
    }
}
