﻿using AoI.TravelSolver.DataTransferObjects;

namespace AoI.TravelSolver.WebApi.Services
{
    public interface IGeneticAlgorithmService
    {
        public PlanRouteResultDto StartGeneticAlgorithm(PlanRouteDto planRouteDto);
    }
}