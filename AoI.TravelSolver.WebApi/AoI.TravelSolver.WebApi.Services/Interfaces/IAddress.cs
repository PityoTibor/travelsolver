﻿using AoI.TravelSolver.DataTransferObjects;
using AoI.TravelSolver.DataTransferObjects.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services
{
    public interface IAddress
    {
        public double getXCoordinate { get; set; }
        public double getYCoordinate { get; set; }

        double CreateDistanceToDoubleFromMatrix(double[,] distanceMatrix, IAddress[] addresses, IAddress targetAddress); //CreateDistanceToDoubleFromMatrix
        void FillAddressAndRequestArrays(IPlanRouteDto planRouteDto);

    }
}
