﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services.Interfaces
{
    public interface IIndividual
    {
        public int[] getChromosome { get; }

        public int getChromosomeLength { get; }

        public bool containsGene(int gene);

        public void setGene(int gene, int offset);

        public int getGene(int offset);

        public void setFitness(double fitness);

        public double getFitness { get; }
    }
}
