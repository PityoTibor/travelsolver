﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services
{
    public static class ApiRequests
    {
        public static RestClient Client()
        { 
            return new RestClient("http://localhost:8080/ors/v2/matrix/driving-car");
        }

        public static RestRequest Request()
        {
            return new RestRequest(Method.POST);
        }
    }
}
