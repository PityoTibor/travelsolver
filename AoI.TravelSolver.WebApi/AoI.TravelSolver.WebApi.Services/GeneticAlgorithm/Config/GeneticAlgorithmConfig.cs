﻿using System.Text.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace AoI.TravelSolver.WebApi.Services
{
    public class GeneticAlgorithmConfig
    {
        public int PopulationSize { get; set; }
        public double MutationRate { get; set; }
        public double CrossoverRate { get; set; }
        public int ElitismCount { get; set; }
        public int TournamentSize { get; set; }
    }
}
