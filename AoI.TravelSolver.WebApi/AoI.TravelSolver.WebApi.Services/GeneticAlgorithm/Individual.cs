﻿using AoI.TravelSolver.WebApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services
{
    public class Individual : IIndividual
    {
        private int[] chromosome;
        private double fitness = -1;

        // Initializes individual with specific chromosome
        public Individual(int[] chromosome)
        {
            // Create individual chromosome
            this.chromosome = chromosome;
        }

        public Individual(int chromosomeLength)
        {
            // Create random individual
            int[] chromosome = new int[chromosomeLength];

            for (int gene = 0; gene < chromosomeLength; gene++)
            {
                chromosome[gene] = gene;
            }

            for (int geneIndex = 0; geneIndex < chromosomeLength; geneIndex++)
            {
                var rnd = new Random();
                // Shuffle genes
                int newGeneIndex = (int)(rnd.NextDouble() * chromosomeLength);
                // Swap individuals
                int tempIndividual = chromosome[newGeneIndex];
                chromosome[newGeneIndex] = chromosome[geneIndex];
                chromosome[geneIndex] = tempIndividual;
            }

            this.chromosome = chromosome;
        }

        public int[] getChromosome { get { return this.chromosome; } }

        public int getChromosomeLength { get { return this.chromosome.Length; } }

        public bool containsGene(int gene)
        {
            // Loop over genes
            for (int i = 0; i < this.getChromosomeLength; i++)
            {
                // Check for gene
                if (this.getGene(i) == gene)
                {
                    return true;
                }
            }
            return false;
        }

        public void setGene(int gene, int offset)
        {
            this.chromosome[offset] = gene;
        }

        public int getGene(int offset)
        {
            return this.chromosome[offset];
        }

        public void setFitness(double fitness)
        {
            this.fitness = fitness;
        }
        public double getFitness { get { return this.fitness; } }
    }
}