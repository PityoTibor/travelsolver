﻿using AoI.TravelSolver.WebApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services
{
    public class GeneticAlgorithm
    {
        //private int populationSize;
        //private double mutationRate;
        //private double crossoverRate;
        //private int elitismCount;
        //private int tournamentSize;
        private GeneticAlgorithmConfig _config;
        private double[,] distanceMatrix;
        private Address mainAddress;

        public GeneticAlgorithm(/*int populationSize, double mutationRate, double crossoverRate, int elitismCount, int tournamentSize,*/ GeneticAlgorithmConfig config, double[,] distanceMatrix, Address mainAddress)
        {
            //this.populationSize = populationSize;
            //this.mutationRate = mutationRate;
            //this.crossoverRate = crossoverRate;
            //this.elitismCount = elitismCount;
            //this.tournamentSize = tournamentSize;
            _config = config;
            this.distanceMatrix = distanceMatrix;
            this.mainAddress = mainAddress;
        }

        // Initialize population
        public Population initPopulation(int chromosomeLength)
        {
            Population population = new Population(_config.PopulationSize, chromosomeLength);
            return population;
        }

        // Calculate individuals fitness value.
        public double calcFitness(IIndividual individual, Address[] addresses)
        {
            // Get fitness
            Route route = new Route(individual, addresses, mainAddress);
            double fitness = 1 / route.getDistance(distanceMatrix, addresses);

            // Store fitness
            individual.setFitness(fitness);

            return fitness;
        }

        // Evaluate population
        public void evalPopulation(Population population, Address[] addresses)
        {
            double totalPoplutaionFitness = 0;

            foreach (var individual in population.getIndividuals())
            {
                totalPoplutaionFitness += this.calcFitness(individual, addresses);
            }

            double avgFitness = totalPoplutaionFitness / population.getPopulationSize();
            population.setAvgFitness(avgFitness);
        }

        // Check if population has met termination condition
        public bool isTerminationConditionMet(int generatinsCount, int maxGenerations)
        {
            return (generatinsCount > maxGenerations);
        }        
        public bool isTerminationConditionMet2(GeneticAlgorithmIsTerminationConditionMetConfig config)
        {
            return (config.generatinsCount > config.maxGenerations);
        }

        // Selects parent for crossover using tournament selection
        public IIndividual tournamentSelection(Population population)
        {
            // Create tournament
            Population tournament = new Population(_config.TournamentSize);

            // Add random individuals to the tournament
            population.shuffle();
            for (int i = 0; i < _config.TournamentSize; i++)
            {
                IIndividual tournamentIndividual = population.getIndividual(i);
                tournament.setIndividual(tournamentIndividual, i);
            }

            // Return the best
            return tournament.getFittest(0);
        }

        // Select parent for crossover
        public IIndividual selectParent(Population population)
        {
            // Get individuals
            IIndividual[] individuals = population.getIndividuals();

            // Spin roulette wheel
            double populationFitness = population.getPopulationFitness();
            var rnd = new Random();
            double rouletteWheelPosition = rnd.NextDouble() * populationFitness;

            // Find parent
            double spinWheel = 0;
            foreach (var individual in individuals)
            {
                spinWheel += individual.getFitness;
                if (spinWheel >= rouletteWheelPosition)
                {
                    return individual;
                }
            }

            return individuals[population.getPopulationSize() - 1];
        }

        // Apply mutation to population
        public Population mutatePopulation(Population population)
        {
            // Initialize new population
            Population newPopulation = new Population(_config.PopulationSize);

            // Loop over current population by fitness
            for (int populationIndex = 0; populationIndex < population.getPopulationSize(); populationIndex++)
            {
                IIndividual individual = population.getFittest(populationIndex);

                // Skip mutation if this is an elite individual
                if (populationIndex > _config.ElitismCount)
                {
                    // Loop over individual's genes
                    var rnd = new Random();
                    for (int geneIndex = 0; geneIndex < individual.getChromosomeLength; geneIndex++)
                    {
                        // Does this gene need mutation?
                        if (_config.MutationRate > rnd.NextDouble())
                        {
                            // Get new gene position
                            int newGenePosition = (int)(rnd.NextDouble() * individual.getChromosomeLength);

                            // Get genes to swap
                            int gene1 = individual.getGene(newGenePosition);
                            int gene2 = individual.getGene(geneIndex);

                            // Swap genes;
                            individual.setGene(gene1, geneIndex);
                            individual.setGene(gene2, newGenePosition);
                        }
                    }
                }

                // Add individual to population
                newPopulation.setIndividual(individual, populationIndex);
            }

            // Return mutated population
            return newPopulation;
        }

        // Order crossover mutation to population
        public Population orderedCrossover(Population population)
        {
            // Create new population
            Population newPopulation = new Population(population.getPopulationSize());

            // Loop over current population by fitness
            for (int populationIndex = 0; populationIndex < population.getPopulationSize(); populationIndex++)
            {
                // Get parent1
                IIndividual parent1 = population.getFittest(populationIndex);

                // Apply crossover to this individual?
                var rnd = new Random();
                if (_config.CrossoverRate > rnd.NextDouble() && populationIndex > _config.ElitismCount)
                {
                    // Find parent2 with tournament selection
                    IIndividual parent2 = this.tournamentSelection(population);

                    // Create blank offspring chromosome
                    int[] offspringChromosome = new int[parent1.getChromosomeLength];
                    Array.Fill(offspringChromosome, -1);
                    Individual offspring = new Individual(offspringChromosome);

                    // Get subset of parent chromosomes
                    int substrPosition1 = (int)(rnd.NextDouble() * parent1.getChromosomeLength);
                    int substrPosition2 = (int)(rnd.NextDouble() * parent1.getChromosomeLength);

                    // make the smaller the start and the larger the end 
                    int startSubstr = Math.Min(substrPosition1, substrPosition2);
                    int endSubstr = Math.Max(substrPosition1, substrPosition2);

                    // Loop and add the sub tour from parent1 to our child
                    for (int i = startSubstr; i < endSubstr; i++)
                    {
                        offspring.setGene(parent1.getGene(i), i);
                    }

                    // Loop through parent2's address tour
                    for (int i = 0; i < parent2.getChromosomeLength; i++)
                    {
                        int parent2Gene = i + endSubstr;
                        if (parent2Gene >= parent2.getChromosomeLength)
                        {
                            parent2Gene -= parent2.getChromosomeLength;
                        }

                        // If offspring doesn't have the address add it
                        if (offspring.containsGene(parent2.getGene(parent2Gene)) == false)
                        {
                            // Loop to find a spare position in the child's tour
                            for (int j = 0; j < offspring.getChromosomeLength; j++)
                            {
                                // Spare position found, add address
                                if (offspring.getGene(j) == -1)
                                {
                                    offspring.setGene(parent2.getGene(parent2Gene), j);
                                    break;
                                }
                            }
                        }
                    }

                    // Add child
                    newPopulation.setIndividual(offspring, populationIndex);
                }
                else
                {
                    // Add individual to new population without applying crossover
                    newPopulation.setIndividual(parent1, populationIndex);
                }
            }

            return newPopulation;
        }
    }
}
