﻿using AoI.TravelSolver.WebApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services
{
    public class Population
    {

        private IIndividual[] population;
        private double avgFitness = -1;

        // Initializes blank population of individuals
        public Population(int populationSize)
        {
            // Initial population
            this.population = new Individual[populationSize];
        }

        // Initializes population of individuals
        public Population(int populationSize, int chromosomeLength)
        {
            // Initial population
            this.population = new Individual[populationSize];

            // Loop over population size
            for (int individualCount = 0; individualCount < populationSize; individualCount++)
            {
                // Create individual
                Individual individual = new Individual(chromosomeLength);
                // Add individual to population
                this.population[individualCount] = individual;
            }
        }

        public IIndividual[] getIndividuals()
        {
            return this.population;
        }

        public IIndividual getFittest(int offset)
        {
            // Order population by fitness
            IIndividual[] orderedPopulation = population.OrderByDescending(item => item.getFitness).ToArray();
            return orderedPopulation[offset];
        }

        public void shuffle()
        {
            // Run through each individual
            for (int individualIndex = 0; individualIndex < this.getPopulationSize(); individualIndex++)
            {
                var rnd = new Random();
                // Get new, random, index for individual
                int index = (int)(rnd.NextDouble() * this.getPopulationSize());
                // Swap individuals
                IIndividual tempIndividual = this.population[index];
                this.population[index] = this.population[individualIndex];
                this.population[individualIndex] = tempIndividual;
            }
        }

        public void setAvgFitness(double fitness)
        {
            this.avgFitness = fitness;
        }

        public double getPopulationFitness()
        {
            return this.avgFitness;
        }

        public int getPopulationSize()
        {
            return this.population.Length;
        }

        public IIndividual setIndividual(IIndividual individual, int offset)
        {
            return population[offset] = individual;
        }

        public IIndividual getIndividual(int offset)
        {
            return population[offset];
        }
    }
}

