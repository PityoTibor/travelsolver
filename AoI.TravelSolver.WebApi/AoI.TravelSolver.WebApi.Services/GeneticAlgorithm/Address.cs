﻿using AoI.TravelSolver.DataTransferObjects;
using AoI.TravelSolver.DataTransferObjects.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services
{
    public class Address : IAddress
    {
        private double xCoordinate;
        private double yCoordinate;
        private static int PlanRouteAddressCount = new PlanRouteDto().Addresses.Count(); // itt lehet hiba


        public Address[] GetAddressesArray { get; set; } = new Address[PlanRouteAddressCount];
        public Address[] GetRequestAddressesArray { get; set; } = new Address[PlanRouteAddressCount + AddPlaceForMainAddress()];

        public double getXCoordinate { get { return this.xCoordinate; } set { } }
        public double getYCoordinate { get { return this.yCoordinate; } set { } }


        public Address(double xCoordinate, double yCoordinate)
        {
            this.xCoordinate = xCoordinate;
            this.yCoordinate = yCoordinate;
        }
        public Address()
        {

        }

        public double CreateDistanceToDoubleFromMatrix(double[,] distanceMatrix, IAddress[] addresses, IAddress targetAddress) //CreateDistanceToDoubleFromMatrix
        {
            int xIndex = 0;
            int yIndex = 0;
            int index = 0;
            foreach (var address in addresses)
            {
                if (this.xCoordinate == addresses[index].getXCoordinate && this.yCoordinate == addresses[index].getYCoordinate)
                {
                    xIndex = index;
                }
                if (targetAddress.getXCoordinate == addresses[index].getXCoordinate && targetAddress.getYCoordinate == addresses[index].getYCoordinate)
                {
                    yIndex = index;
                }
                index++;
            }
            double distance = distanceMatrix[xIndex, yIndex];
            return distance;
        }

        public void FillAddressAndRequestArrays(IPlanRouteDto planRouteDto)
        {
            int index = 0;
            foreach (var address in planRouteDto.Addresses)
            {
                GetAddressesArray[index] = new Address(address.xCoordinate, address.yCoordinate);
                GetRequestAddressesArray[index] = new Address(address.xCoordinate, address.yCoordinate);
                index++;
            }
        }


        private static int AddPlaceForMainAddress()
        {
            return 1;
        }
    }
}
