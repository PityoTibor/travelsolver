﻿using AoI.TravelSolver.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services
{
    public class ComputeGeneticAlgorithm
    {
        
        private Address[] finalAdresses;
        private double finalDistance;
        private Address s = new Address();

        //public PlanRouteResultDto Genera
        public void StartGeneticAlgorithm(GeneticAlgorithmConfig config, double[,] DistanceMatrix, Address MainAddress)
        {
            GeneticAlgorithm ga = new GeneticAlgorithm(config, DistanceMatrix, MainAddress);
            Population population = ga.initPopulation(s.GetAddressesArray.Length);
            
            Route startRoute = new Route(population.getFittest(0), s.GetAddressesArray, MainAddress);
            int generation = 0;
            Route finalRoute = startRoute;
            while (ga.isTerminationConditionMet(generation, 200) == false)
            {
                // Get fittest individual from population
                finalRoute = new Route(population.getFittest(0), s.GetAddressesArray, MainAddress);

                // Apply crossover
                population = ga.orderedCrossover(population);

                // Apply mutation
                population = ga.mutatePopulation(population);

                // Evaluate population
                ga.evalPopulation(population, s.GetAddressesArray);

                // Increment the current generation
                generation++;
            }

            finalAdresses = (Address[])finalRoute.getRoute();
            finalDistance = finalRoute.getDistance(DistanceMatrix, s.GetAddressesArray);
        }

        public PlanRouteResultDto GetPlanRouteResult(IAddress MainAddress)
        {
            List<LatLngDto> latLngDtos = new List<LatLngDto>();
            latLngDtos.Add(new LatLngDto(MainAddress.getXCoordinate, MainAddress.getYCoordinate));
            foreach (var address in finalAdresses)
            {
                latLngDtos.Add(new LatLngDto(MainAddress.getXCoordinate, MainAddress.getYCoordinate));
            }
            latLngDtos.Add(new LatLngDto(MainAddress.getXCoordinate, MainAddress.getYCoordinate));

            return new PlanRouteResultDto(latLngDtos, finalDistance);
        }
    }
}
