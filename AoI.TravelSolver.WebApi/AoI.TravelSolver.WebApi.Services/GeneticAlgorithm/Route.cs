﻿using AoI.TravelSolver.WebApi.Services;
using AoI.TravelSolver.WebApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoI.TravelSolver.WebApi.Services
{
    public class Route
    {
        private IAddress[] route;
        private IAddress mainAddress;

        public Route(IIndividual individual, IAddress[] addresses, IAddress mainAddress)
        {
            // Get individual's chromosome
            int[] chromosome = individual.getChromosome;
            // Create route
            this.route = new Address[addresses.Length];
            for (int geneIndex = 0; geneIndex < chromosome.Length; geneIndex++)
            {
                this.route[geneIndex] = addresses[chromosome[geneIndex]];
            }
            this.mainAddress = mainAddress;
        }
        public IAddress[] getRoute()
        {
            return route;
        }

        public double getDistance(double[,] distanceMatrix, Address[] addresses)
        {
            double distance = 0;
            if (distance > 0)
            {
                return distance;
            }
            Address[] matrixAddresses = new Address[addresses.Length + 1];
            int index = 0;
            foreach (var address in addresses)
            {
                matrixAddresses[index] = addresses[index];
                index++;
            }
            
            matrixAddresses[index] = (Address)mainAddress;
            // Loop over cities in route and calculate route distance
            double totalDistance = 0;
            totalDistance += mainAddress.CreateDistanceToDoubleFromMatrix(distanceMatrix, matrixAddresses, this.route[0]);
            for (int addressIndex = 0; addressIndex + 1 < this.route.Length; addressIndex++)
            {
                //totalDistance += this.route[addressIndex].distanceTo(this.route[addressIndex+1]);
                totalDistance += this.route[addressIndex].CreateDistanceToDoubleFromMatrix(distanceMatrix, matrixAddresses, this.route[addressIndex + 1]);
            }

            //totalDistance += this.route[this.route.Length - 1].distanceTo(this.route[0]);
            totalDistance += this.route[this.route.Length - 1].CreateDistanceToDoubleFromMatrix(distanceMatrix, matrixAddresses, mainAddress);
            distance = totalDistance;

            return distance;
        }

        public override string ToString()
        {
            string routeString = "";
            foreach (var item in route)
            {
                routeString += "(" + item.getXCoordinate + "," + item.getYCoordinate + ")";
            }
            return routeString;
        }
    }
}
